import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.spec.ECPoint;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.InvalidKeySpecException;

import javax.xml.bind.DatatypeConverter;

import org.bouncycastle.asn1.*;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;

public class Main {

    public static void main(String[] args) throws Exception {

        String publicKeyHex = "f5f2e25a89f09dfc222402a2af203ec3aad2e576e245b774cef65ad4d026660a990cb6ae8bc79059155fa9e1c5bfe93d9fb7b6e38eed70fddf1d5699dc2aca4f";
        String signatureHex = "23a1c2da68ecee7cc95d116292822e81c01faaf38078af7b6dbfaf86d55a2cb323c36d07e0f98ae49f430cb4659326ad6f7c06a6d2d8a07f62b9da6b2e99ff4a";
        String messageStr = "0xeD432baeeD91aa9a4e4c543EfD829f5Eac9421de";

        BigInteger x = new BigInteger(1, toByteArray(publicKeyHex.substring(0, publicKeyHex.length() / 2)));
        BigInteger y = new BigInteger(1, toByteArray(publicKeyHex.substring(publicKeyHex.length() / 2)));
        BigInteger r = new BigInteger(1, toByteArray(signatureHex.substring(0, signatureHex.length() / 2)));
        BigInteger s = new BigInteger(1, toByteArray(signatureHex.substring(signatureHex.length() / 2)));

        System.out.println("X: " + x);
        System.out.println("Y: " + y);
        System.out.println("r: " + r);
        System.out.println("s: " + s);

        // Generate a PublicKey from the HEX string received by the HW wallet (Stored in the DB).
        PublicKey pub = generatePublicKey(publicKeyHex);

        // Get a DER encoded byte string of the signature returned by the wallet (Provided by SDK).
        byte[] signature_bin = encodeSignatureToDer(signatureHex);

        // Verify the signature using the public key and the signed message.
        Boolean verification = verify(pub, signature_bin, messageStr.getBytes());

        System.out.println("Verification result: " + verification);
    }

    public static PublicKey generatePublicKey(String publicKeyHex) throws InvalidKeySpecException, NoSuchAlgorithmException {
        BigInteger x = new BigInteger(1, toByteArray(publicKeyHex.substring(0, publicKeyHex.length() / 2)));
        BigInteger y = new BigInteger(1, toByteArray(publicKeyHex.substring(publicKeyHex.length() / 2)));
        ECNamedCurveParameterSpec spec = ECNamedCurveTable.getParameterSpec("prime256v1");
        KeyFactory kf = KeyFactory.getInstance("EC", new BouncyCastleProvider());
        ECNamedCurveSpec params = new ECNamedCurveSpec("prime256v1", spec.getCurve(), spec.getG(), spec.getN());
        ECPoint point = new ECPoint(x, y);
        ECPublicKeySpec pubKeySpec = new ECPublicKeySpec(point, params);
        return kf.generatePublic(pubKeySpec);
    }

    public static boolean verify(PublicKey publicKey, byte[] signed, byte[] message) throws Exception {
        Signature signature = Signature.getInstance("SHA256withECDSA");
        signature.initVerify(publicKey);
        signature.update(message);
        return signature.verify(signed);
    }

    public static String toHexString(byte[] array) {
        return DatatypeConverter.printHexBinary(array);
    }

    public static byte[] toByteArray(String s) {
        return DatatypeConverter.parseHexBinary(s);
    }

    public static byte[] encodeSignatureToDer(String signatureHex) throws IOException {
        BigInteger r = new BigInteger(1, toByteArray(signatureHex.substring(0, signatureHex.length() / 2)));
        BigInteger s = new BigInteger(1, toByteArray(signatureHex.substring(signatureHex.length() / 2)));
        ByteArrayOutputStream bos = new ByteArrayOutputStream(72);
        DERSequenceGenerator seq = new DERSequenceGenerator(bos);
        seq.addObject(new ASN1Integer(r));
        seq.addObject(new ASN1Integer(s));
        seq.close();
        return bos.toByteArray();
    }
}
